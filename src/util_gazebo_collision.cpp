#include <ros/ros.h>
#include <ros/console.h>

#include <std_msgs/Bool.h>

#include <gazebo/gazebo_config.h>
#include <gazebo/gazebo_client.hh>
#include <gazebo/transport/transport.hh>

class CheckCollision {
private:
  // ros NodeHandle and loop rate
  ros::NodeHandle n;

  // parameters
  std::string environment;

  // subscribers
  gazebo::transport::SubscriberPtr sub_contacts;

  // publisher
  ros::Publisher collision = n.advertise<std_msgs::Bool>("collision", 1000);

public:
  CheckCollision(){

    // gazebo transport node initialization
    gazebo::transport::NodePtr node(new gazebo::transport::Node());
    node->Init();

    // subscribers
    sub_contacts = node->Subscribe("/gazebo/default/physics/contacts", &CheckCollision::contactsCallback, this);

    // parameters
    n.getParam("check_collision/environment", environment);

  }
  ~CheckCollision(){
    gazebo::client::shutdown();
  }

  void start(){

    ROS_INFO_STREAM("environment: " << environment);

    ros::spin();

  }


  void contactsCallback(ConstContactsPtr &_msg){
    int cont = 0;
    for (size_t i = 0; i < _msg->contact_size() ; i++) {
      std::string collision_1 = _msg->contact(i).collision1();
      std::string collision_2 = _msg->contact(i).collision2();

      if (collision_1.rfind("ground_plane", 0) == 0 or
          collision_2.rfind("ground_plane", 0) == 0 or
          collision_1.rfind(environment+"::floor::floor_collision", 0) == 0 or
          collision_2.rfind(environment+"::floor::floor_collision", 0) == 0){
        continue;
      }
      cont++;
    }

    if(cont > 0){
      // ROS_INFO_STREAM("Colllision");
      std_msgs::Bool b;
      b.data = true;
      collision.publish(b);
    }
  }

};


int main(int argc, char **argv){

  // init gazebo and ros
  ros::init(argc, argv, "gazebo_collision");
  gazebo::client::setup(argc, argv);

  // start node
  CheckCollision* c = new CheckCollision();
  c->start();

  return 0;
}
