#!/usr/bin/env python
import rospy
import yaml

from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import OccupancyGrid
from util_ros.srv import PointClear, PointClearResponse

# from visualization_msgs.msg import Marker, MarkerArray
# from geometry_msgs.msg import Quaternion, Pose, Point, Vector3
# from std_msgs.msg import ColorRGBA

class UtilMoveBase():
    """docstring for UtilMoveBase."""
    def __init__(self):

        self.grid_global = None
        self.grid_local = None
        self.rate = rospy.Rate(10)

        rospy.Subscriber("/move_base/global_costmap/costmap", OccupancyGrid, self.callback_global_costmap)
        rospy.Subscriber("/move_base/local_costmap/costmap", OccupancyGrid, self.callback_local_costmap)
        rospy.Subscriber("/move_base_simple/goal", PoseStamped, self.callback_goal)
        rospy.Service('~point_clear_global', PointClear, self.handle_point_clear_global)
        rospy.Service('~point_clear_local', PointClear, self.handle_point_clear_local)

        # self.pub = rospy.Publisher("cloud", MarkerArray)

        while not rospy.is_shutdown():
            # self.print_cloud()
            self.rate.sleep()

        # rospy.spin()

    def callback_global_costmap(self, data):
        self.grid_global = data

    def callback_local_costmap(self, data):
        self.grid_local = data

    def callback_goal(self, data):
        result = self.point_clear_local(data.pose.position)
        clear = "clear." if result else "not clear."
        rospy.loginfo("The point (" +
            str(data.pose.position.x) + "," +
            str(data.pose.position.y) + ") is " + clear)

    def handle_point_clear_local(self, req):
        result = self.point_clear_local(req.position)
        clear = "clear." if result else "not clear."
        rospy.loginfo("The local point (" +
            str(req.position.x) + "," +
            str(req.position.y) + ") is " + clear)
        return PointClearResponse(result)

    def handle_point_clear_global(self, req):
        result = self.point_clear_global(req.position)
        clear = "clear." if result else "not clear."
        rospy.loginfo("The global point (" +
            str(req.position.x) + "," +
            str(req.position.y) + ") is " + clear)
        return PointClearResponse(result)

    def point_clear_local(self, point):
        if(self.grid_local is None):
            raise Exception('local costmap not received.')
        else:
            x = int((point.x - self.grid_local.info.origin.position.x) / self.grid_local.info.resolution)
            y = int((point.y - self.grid_local.info.origin.position.y) / self.grid_local.info.resolution)

            if(x < 0 or x > self.grid_local.info.width
            or y < 0 or y > self.grid_local.info.height):
                return True

            a = self.grid_local.data[x+self.grid_local.info.width*y]
            print "aa " + str(a)
            result = (int(a) < 10)
            print result
            return result


    def point_clear_global(self, point):
        if(self.grid_global is None):
            raise Exception('global costmap not received.')
        else:
            x = int((point.x - self.grid_global.info.origin.position.x) / self.grid_global.info.resolution)
            y = int((point.y - self.grid_global.info.origin.position.y) / self.grid_global.info.resolution)

            if(x < 0 or x > self.grid_global.info.width
            or y < 0 or y > self.grid_global.info.height):
                return True

            a = self.grid_global.data[x+self.grid_global.info.width*y]
            result = (a < 100)
            return result


if __name__ == '__main__':
    rospy.init_node('util_move_base', anonymous=True)
    UtilMoveBase()
