#!/usr/bin/env python

import rospy
import tf

from people_msgs.msg import PositionMeasurementArray, PositionMeasurement
br = tf.TransformBroadcaster()
people = PositionMeasurement()

def callback_people(data):
    prople = data.people

    for p in data.people:
        br.sendTransform((p.pos.x, p.pos.y, p.pos.z),
            tf.transformations.quaternion_from_euler(0, 0, 0),
            rospy.Time.now(),
            p.name,
            p.header.frame_id)


    # p1 = None
    # for p2 in data.people:
    #     if (p1==None):
    #         p1 = p2
    #     else:
    #         try:
    #             (trans,rot) = self.tf_listener.lookupTransform('/map', self.tfTarget, rospy.Time(0))
    #             angular = math.atan2(trans[1], trans[0])
    #             linear =  math.sqrt(trans[0] ** 2 + trans[1] ** 2)
    #             (x,y) = pol2cart(linear,angular)
    #
    #             a = numpy.array([x,y])
    #             b1 = numpy.array([p1.pos.x,p1.pos.y])
    #             b2 = numpy.array([p2.pos.x,p2.pos.y])
    #             c1 = a-b1
    #             c2 = a-b2
    #
    #             dist1 = math.sqrt(c1[0]**2 + c1[1]**2)
    #             dist2 = math.sqrt(c2[0]**2 + c2[1]**2)
    #
    #             if (dist2 < dist1):
    #                 p1 = p2
    #
    #         except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
    #             rospy.logwarn('tf error.')
    #
    # if (p1 is not None):
    #     target = Pose()
    #     target.position.x = p1.pos.x
    #     target.position.y = p1.pos.y
    #     target.position.z = p1.pos.z
    #     target.orientation.x = 0
    #     target.orientation.y = 0
    #     target.orientation.z = 0
    #     target.orientation.w = 1
    #     self.pf.target = target
    # else:
    #     self.pf.target = Nonens,rot) = self.tf_listener.lookupTransform('/map', self.tfTarget, rospy.Time(0))
    #             angular = math.atan2(trans[1], trans[0])
    #             linear =  math.sqrt(trans[0] ** 2 + trans[1] ** 2)
    #             (x,y) = pol2cart(linear,angular)
    #
    #             a = numpy.array([x,y])
    #             b1 = numpy.array([p1.pos.x,p1.pos.y])
    #             b2 = numpy.array([p2.pos.x,p2.pos.y])
    #             c1 = a-b1
    #             c2 = a-b2
    #
    #             dist1 = math.sqrt(c1[0]**2 + c1[1]**2)
    #             dist2 = math.sqrt(c2[0]**2 + c2[1]**2)
    #
    #             if (dist2 < dist1):
    #                 p1 = p2
    #
    #         except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
    #             rospy.logwarn('tf error.')
    #
    # if (p1 is not None):
    #     target = Pose()
    #     target.position.x = p1.pos.x
    #     target.position.y = p1.pos.y
    #     target.position.z = p1.pos.z
    #     target.orientation.x = 0
    #     target.orientation.y = 0
    #     target.orientation.z = 0
    #     target.orientation.w = 1
    #     self.pf.target = target
    # else:
    #     self.pf.target = None

if __name__ == "__main__":
    rospy.init_node('util_leg_detector', anonymous=True)

    rospy.Subscriber("/people_tracker_measurements", PositionMeasurementArray, callback_people)

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
