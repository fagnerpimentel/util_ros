#!/usr/bin/env python

import rospy
import numpy
import math
import tf
from sensor_msgs.msg import LaserScan
from vectors import Vector


# parent frame
frame_id = 'base_link'

# [<topic>,<frame>]
# scans_input = [['scan','base_laser_front_link'],['scan_back','base_laser_back_link']]
scans_input = [['scan','base_laser_front_link']]
scan_output =['scan_combined','scan_combined_link']

def pol2cart(rho, phi):
    x = rho * numpy.cos(phi)
    y = rho * numpy.sin(phi)
    return(x, y)

def cart2pol(x, y):
    rho = numpy.sqrt(x**2 + y**2)
    phi = numpy.arctan2(y, x)
    return(rho, phi)

class UtilScanFusion():
    """docstring for UtilScanFusion."""
    def __init__(self):
        broadcaster = tf.TransformBroadcaster()
        listener = tf.TransformListener()
        rate = rospy.Rate(10)
        pub = rospy.Publisher(scan_output[0], LaserScan, queue_size=10)
        for i in range(0,len(scans_input)):
            rospy.Subscriber(scans_input[i][0], LaserScan, self.callback_laser_scan, i)

        # scan list
        self.ls = [None] * len(scans_input)

        # publish new scan_tf
        trans = []; rot = []
        while(True):
            try:
                rate.sleep()
                for i in range(0,len(scans_input)):
                    (t,r) = listener.lookupTransform(frame_id, scans_input[i][1], rospy.Time(0))
                    trans.append(t)
                    rot.append(r)
                position = numpy.sum(trans,0)/2
                broadcaster.sendTransform(
                    (position[0], position[1], position[2]),
                    (0.0, 0.0, 0.0, 1.0),
                    rospy.Time.now(),scan_output[1],frame_id)
                break
            except Exception as e:
                print e


        # main loop
        seq = 0.0
        while not rospy.is_shutdown():
            rate.sleep()

            # if scan list not updated
            if(not all(self.ls)):
                continue

            # create scan
            seq += 1
            ls_combined = LaserScan()
            ls_combined.header.seq = seq
            ls_combined.header.stamp = rospy.Time.now()
            ls_combined.header.frame_id = frame_id
            ls_combined.angle_min = -math.pi
            ls_combined.angle_max = math.pi
            ls_combined.angle_increment = min([x.angle_increment for x in self.ls])
            ls_combined.time_increment = min([x.time_increment for x in self.ls])
            ls_combined.scan_time = min([x.scan_time for x in self.ls])
            ls_combined.range_min = min([x.range_min for x in self.ls])
            ls_combined.range_max = min([x.range_max for x in self.ls])

            # initialize scan data
            for i in numpy.arange(ls_combined.angle_min, ls_combined.angle_max,ls_combined.angle_increment):
                ls_combined.ranges.append(numpy.nan)
                # ls_combined.intensities.append(numpy.nan)

            # publish new scan_tf
            trans = []; rot = []
            for i in range(0,len(scans_input)):
                (t,r) = listener.lookupTransform(frame_id, scans_input[i][1], rospy.Time(0))
                trans.append(t)
                rot.append(r)
            position = numpy.sum(trans,0)/2
            broadcaster.sendTransform(
                (position[0], position[1], position[2]),
                (0.0, 0.0, 0.0, 1.0),
                rospy.Time.now(),scan_output[1],frame_id)

            # transform each scan
            for i in range(0,len(scans_input)):
                (t,r) = listener.lookupTransform(scans_input[i][1], scan_output[1], rospy.Time(0))

                quaternion = [rot[i][0], rot[i][1], rot[i][2], rot[i][3]]
                euler = tf.transformations.euler_from_quaternion(quaternion)

                for j in range(0,len(self.ls[i].ranges)):
                    k = (ls_combined.angle_min +
                        euler[2] + self.ls[i].angle_min +
                        j * self.ls[i].angle_increment)
                    kk =int((k%(2*math.pi))/ls_combined.angle_increment)
                    x, y = pol2cart(self.ls[i].ranges[j], (k%(2*math.pi)))
                    v1 = Vector(t[0],t[1],0.0)
                    v2 = Vector(x, y, 0.0)
                    v3 = v1.sum(v2)
                    ls_combined.ranges[kk] = v3.magnitude()
                    # ls_combined.ranges[kk] = self.ls[i].ranges[j]
                    # ls_combined.intensities[kk] = self.ls[i].intensities

            # publish new scan
            pub.publish(ls_combined)


            # clear data
            self.ls = [None] * len(scans_input)

    def callback_laser_scan(self,data,args):
        self.ls[args] = data


if __name__ == '__main__':
    rospy.init_node('util_scan_fusion', anonymous=True)
    UtilScanFusion()
